import { Client } from "https://deno.land/x/mysql/mod.ts";

async function openDbConnection(dbHost, dbUsername, dbPassword, callback) {
  const client = await new Client().connect({
    db: "colcomercio_geo",
    hostname: dbHost,
    username: dbUsername,
    poolSize: 3,
    password: dbPassword,
  });

  await callback(client);
  await client.close();
}

async function upsertNeighborhood(client, neighborhood) {
  await client.execute(
    `INSERT INTO colcomercio_geo.neighborhoods (code, city_code, name, updated, active)
        SELECT * FROM (SELECT '${neighborhood.code.trim()}' AS code, '${neighborhood.city_code.trim()}' AS city_code, '${neighborhood.name.trim()}' AS name, '${neighborhood.updated.trim()}' AS updated, '${neighborhood.active.trim()}' AS active) AS temp
        WHERE NOT EXISTS (SELECT * FROM colcomercio_geo.neighborhoods WHERE code = '${
          neighborhood.code
        }') LIMIT 1;
        `
  );
}

async function upsertNeighborhoodMass(client, neighborhoods) {
  let query = `INSERT IGNORE INTO colcomercio_geo.neighborhoods (code, city_code, name, updated, active)
  VALUES `;
  for (let neighborhood of neighborhoods) {
    query =
      query +
      `('${neighborhood.code.trim()}', '${neighborhood.city_code.trim()}', '${neighborhood.name.trim()}', '${neighborhood.updated.trim()}', '${neighborhood.active.trim()}'),`;
  }
  query = query.replace(/,$/g, "");
  query = query + `;`;

  let result = await client.execute(query);

  return result;
}

async function upsertCity(client, city) {
  await client.execute(
    `INSERT INTO colcomercio_geo.cities (code, name, depto_name, depto_code, updated, postal_code)
     SELECT * FROM (SELECT '${city.code.trim()}' AS code, '${city.name.trim()}' AS name, '${city.depto_name.trim()}' AS depto_name, '${city.depto_code.trim()}' AS depto_code, '${city.updated.trim()}' AS updated, '${city.postal_code.trim()}' AS postal_code) AS temp
     WHERE NOT EXISTS (SELECT * FROM colcomercio_geo.cities WHERE code = '${
       city.code
     }') LIMIT 1;`
  );
}

async function upsertCityMass(client, cities) {
  let query = "";
  query = `INSERT IGNORE INTO colcomercio_geo.cities (code, name, depto_name, depto_code, updated, postal_code)
  VALUES `;

  for (let city of cities) {
    query =
      query +
      `('${city.code.trim()}', '${city.name.trim()}', '${city.depto_name.trim()}', '${city.depto_code.trim()}', '${city.updated.trim()}', '${city.postal_code.trim()}'),`;
  }

  query = query.replace(/,$/g, "");
  query = query + `;`;

  console.log(query);

  let result = await client.execute(query);

  return result;
}

export {
  upsertNeighborhood,
  upsertCity,
  openDbConnection,
  upsertNeighborhoodMass,
  upsertCityMass,
};
