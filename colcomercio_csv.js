import {
  openDbConnection,
  upsertCityMass,
  upsertNeighborhoodMass,
} from "./colcomercio_geo_db_access.js";

const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const filesDirectory = Deno.env.get("FTP_FILES_DIRECTORY");
const neighborhoodPatternRegex = Deno.env.get("FTP_NEIGHBORHOOD_PATTERN");
const cityPatternRegex = Deno.env.get("FTP_CITY_PATTERN");

const dbHost = Deno.env.get("COLCOMERCIO_DB_HOSTNAME");
const dbUsername = Deno.env.get("COLCOMERCIO_DB_USERNAME");
const dbPassword = Deno.env.get("COLCOMERCIO_DB_PASSWORD");

try {
  await processCityFilesMass();
  await processNeighborhoodFilesMass();
} catch (e) {
  console.log(e.message);
}

async function listFiles(folder, pattern) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();

    const command = `curl  --user '${user}:${pass}' -l -k 'sftp://${ftpHost}:22${folder}' | grep '${pattern}'`;
    await process.stdin.write(encoder.encode(command));

    await process.stdin.close();
    const output = await process.output();
    return decoder
      .decode(output)
      .split(`\n`)
      .filter((x) => {
        return x != null && x != "";
      });
  } catch (e) {
    console.log(`Could not fetch files, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function listFilesMock(folder, pattern) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();

    const command = `curl  --user '${user}:${pass}' -l -k 'ftp://${ftpHost}:21${folder}' | grep '${pattern}'`;
    console.log(`Running command: ${command}`);
    await process.stdin.write(encoder.encode(command));

    await process.stdin.close();
    const output = await process.output();
    return decoder
      .decode(output)
      .split(`\n`)
      .filter((x) => {
        return x != null && x != "";
      });
  } catch (e) {
    console.log(`Could not fetch files, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function getFile(folder, file) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const command = `curl  -k 'sftp://${ftpHost}:22${folder}${file}' --user '${user}:${pass}'`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    const output = await process.output();
    return decoder.decode(output);
  } catch (e) {
    console.log(`Could not fetch file: ${file}, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function getFileMock(folder, file) {
  const process = Deno.run({
    cmd: ["bash"],
    stdout: "piped",
    stdin: "piped",
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const command = `curl  -k 'ftp://${ftpHost}:21${folder}${file}' --user '${user}:${pass}'`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    const output = await process.output();
    return decoder.decode(output);
  } catch (e) {
    console.log(`Could not fetch file: ${file}, error: ${e.message}`);
    throw e;
  } finally {
    process.close();
  }
}

async function processCityFilesMass() {
  console.log("Retrieving data...");
  let data = await listFilesMock(filesDirectory, cityPatternRegex);
  console.log(data);
  for (let file of data) {
    try {
      let cityRows = await getFileMock(filesDirectory, file);

      cityRows = cityRows.split("\n").slice(1, -1);
      let cityList = [];
      for (let cityRow of cityRows) {
        let cityFields = cityRow.replace(/"/g, "").split(";");
        let cityCode = cityFields[0];
        let cityNameAndState = cityFields[1]
          .toLowerCase()
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "");
        let cityStateNameGroup = cityNameAndState.match(/.+(?<gp1>\(.+\))$/)
          ? cityNameAndState.match(/.+(?<gp1>\(.+\))$/).groups.gp1
          : "Disable";
        let cityState = cityStateNameGroup
          .replace("(", "")
          .replace(")", "")
          .trim();
        let cityName = cityNameAndState.replace(cityStateNameGroup, "").trim();
        let cityStateCode = cityFields[2];
        let cityUpdateDate = cityFields[3];
        let cityPostalCode = cityFields[4];
        let city = {
          code: cityCode,
          name: cityName,
          depto_name: cityState,
          depto_code: cityStateCode,
          updated: cityUpdateDate,
          postal_code: cityPostalCode,
        };
        cityList.push(city);
      }
      await openDbConnection(dbHost, dbUsername, dbPassword, async (client) => {
        let result = await upsertCityMass(client, cityList);
        console.log(result);
      });
    } catch (e) {
      console.log(`Unable to get or process file ${file}: ${e.message}`);
    }
  }
}

async function processNeighborhoodFilesMass() {
  console.log("Retrieving data...");
  let data = await listFilesMock(filesDirectory, neighborhoodPatternRegex);
  console.log(data);
  try {
    for (let file of data) {
      let neighborhoodRows = await getFileMock(filesDirectory, file);

      neighborhoodRows = neighborhoodRows.split("\n").slice(1, -1);
      let neighborhoodList = [];
      for (let neighborhoodRow of neighborhoodRows) {
        let neighborhoodFields = neighborhoodRow.replace(/"/g, "").split(";");
        let neighborhoodName = neighborhoodFields[2]
          .toLowerCase()
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "");
        let neighborhood = {
          code: neighborhoodFields[0],
          city_code: neighborhoodFields[1],
          name: neighborhoodName,
          updated: neighborhoodFields[3],
          active: neighborhoodFields[4],
        };
        neighborhoodList.push(neighborhood);
      }
      await openDbConnection(dbHost, dbUsername, dbPassword, async (client) => {
        let result = await upsertNeighborhoodMass(client, neighborhoodList);
        console.log(result);
      });
    }
  } catch (e) {
    console.log(`Unable to get or process file ${file}: ${e.message}`);
  }
}
